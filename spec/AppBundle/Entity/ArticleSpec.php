<?php

namespace spec\AppBundle\Entity;

use AppBundle\Entity\Article;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ArticleSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedThrough('createNew', array(
            'My article',
            'Here goes my introduction',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ));
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('AppBundle\Entity\Article');
    }

    function it_is_an_article()
    {
        $this->shouldImplement(Article::class);
    }

    function it_should_be_able_to_set_title()
    {
        $this->setTitle('My article')->shouldReturn(null);
    }

    function it_should_have_a_title()
    {
        $this->getTitle()->shouldReturn('My article');
    }

    function it_should_be_able_to_set_intro()
    {
        $this->setIntro('Here goes my introduction')->shouldReturn(null);
    }

    function it_should_have_intro()
    {
        $this->getIntro()->shouldReturn('Here goes my introduction');
    }

    function it_should_be_able_to_set_content()
    {
        $this->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit.')->shouldReturn(null);
    }

    function it_should_have_content()
    {
        $this->getContent()->shouldReturn('Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
    }

}
