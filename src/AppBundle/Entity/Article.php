<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Entity;

class Article
{
    /** @var integer */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $intro;

    /** @var string */
    private $content;

    /**
     * @param string $title
     * @param string $intro
     * @param string $content
     * @return Article
     */
    public static function createNew($title, $intro, $content)
    {
        $article = new Article();
        $article->setTitle($title);
        $article->setIntro($intro);
        $article->setContent($content);
        return $article;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $intro
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;
    }

    /**
     * @return string
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}
