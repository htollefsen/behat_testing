<?php

use AppBundle\Entity\User;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\MinkExtension\Context\MinkContext;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;

/**
 * Defines application features from the specific context.
 */
class UserContext extends MinkContext implements SnippetAcceptingContext
{


    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @AfterStep
     */
    public function takeScreenShotAfterFailedStep(afterStepScope $scope)
    {
        if (99 === $scope->getTestResult()->getResultCode()) {
            $driver = $this->getSession()->getDriver();
            if (!($driver instanceof Selenium2Driver)) {
                return;
            }
            file_put_contents('/tmp/test.png', $this->getSession()->getDriver()->getScreenshot());
        }
    }

    /**
     * @BeforeScenario @cleandatabase
     */
    public function cleanDatabase()
    {
        $purger = new ORMPurger($this->entityManager);
        $purger->purge();
    }

    /**
     * @Given I am on :page
     */
    public function iAmOn($page)
    {
        $this->visit($page);
    }

    /**
     * @Then I should see :text
     */
    public function iShouldSee($text)
    {

        $this->assertPageContainsText($text);
    }

    /**
     * @Given user exist with username :username and password :password
     */
    public function userExistWithUsernameAndPassword($username, $password)
    {
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($password);
        $user->setEmail($username . '@example.com');
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @When I press :button
     */
    public function iPress($button)
    {
        $this->pressButton($button);
    }

    /**
     * @When I fill field :fieldName with :fieldValue
     */
    public function iFillFieldWith($fieldName, $fieldValue)
    {
        $this->fillField($fieldName, $fieldValue);
    }

    /**
     * @Then I should be redirected to :path and see :text
     */
    public function iShouldBeRedirectedToAndSee($path, $text)
    {
        $this->getSession()->wait(5000, $this->iAmOn($path));
        $this->iShouldSee($text);
    }

    /**
     * @Given /^I am logged in as an administrator$/
     */
    public function iAmLoggedInAsAnAdministrator()
    {
        $this->userExistWithUsernameAndPassword('admin', 'password');
        $this->iAmOn('/login');
        $this->iFillFieldWith('_username', 'admin');
        $this->iFillFieldWith('_password', 'password');
        $this->iPress('_submit');
        $this->iShouldBeRedirectedToAndSee('/', 'Welcome');
    }
}
