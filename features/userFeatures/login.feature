@javascript @cleandatabase
Feature:
  In order to use administration features
  As an website user
  I must be able to log in

  Scenario:
    Given I am on '/login'
    Given user exist with username 'admin' and password 'password'
    When I fill field '_username' with 'admin'
    When I fill field '_password' with 'password'
    When I press _submit
    Then I should be redirected to '/' and see 'Welcome'
