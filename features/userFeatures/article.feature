@javascript @cleandatabase
Feature:
  In order to display articles
  As a user
  I must be able to create, edit, delete and display articles

  Scenario: Creating an article
    Given I am logged in as an administrator
    Given I am on '/article/create'
    When I fill field 'title' with 'My article'
    When I fill field 'intro' with 'Here goes my introduction'
    When I fill field 'content' with 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
    When I press 'submit'
    Then I should be redirected to '/article/show/1' and see 'My article'

