@javascript @cleandatabase
Feature:
  In order to login in
  As an administration user
  I must be able to register a new user

  Scenario:
    Given I am on '/register'
    When I fill field 'fos_user_registration_form_email' with 'user@example.com'
    When I fill field 'fos_user_registration_form_username' with 'admin'
    When I fill field 'fos_user_registration_form_plainPassword_first' with 'password'
    When I fill field 'fos_user_registration_form_plainPassword_second' with 'password'
    When I press Register
    Then I should be redirected to '/register/confirmed' and see 'Logged in'
