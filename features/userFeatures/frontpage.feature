Feature:
  In order to use the website
  As a website user
  I need to be able to see the frontpage

  Scenario:
    Given I am on '/'
    Then I should see 'Welcome'
